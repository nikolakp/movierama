export class Movie {
    id: number;
    title: string;
    description: string;
    creator: string;
    createdAt: any;
    hates: number;
    likes: number;
}
