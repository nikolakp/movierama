import { Movie } from "./movie";

export class MovieRoot {
    currentPage: number;
    hasNextPage: boolean;
    movies: Movie[];
  }