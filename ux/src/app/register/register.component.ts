import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from '../models/user';
import { MessageService } from '../message.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  users: User[];
  success: boolean;

  constructor(private auth: AuthService, private messageService: MessageService, private router: Router) { 
  }

  ngOnInit() {
    this.users = [];
    this.success = false;
  }

  registerUser(event) {
    event.preventDefault()
    const target = event.target
    const username = target.querySelector('#username').value
    const password = target.querySelector('#password').value
    if (!username || !password) { return; }
    this.auth.registerUser({ username, password } as User)
      .subscribe(user => {
        this.success = true;
        this.router.navigate(['login']) // redirect the person to login
      });
  }
}
