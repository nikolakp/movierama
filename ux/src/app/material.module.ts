import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Form controls
import { MatCheckboxModule } from '@angular/material';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material';
import { MatRadioModule } from '@angular/material';
import { MatSelectModule } from '@angular/material';
import { MatSliderModule } from '@angular/material';
import { MatSlideToggleModule } from '@angular/material';

// Navigation
import { MatMenuModule } from '@angular/material';

// Layout
import { MatCardModule } from '@angular/material';
import { MatDividerModule } from '@angular/material';
import { MatListModule } from '@angular/material';
import { MatTabsModule } from '@angular/material';
import { MatExpansionModule } from '@angular/material';

// Buttons & indicators
import { MatChipsModule } from '@angular/material';
import { MatProgressSpinnerModule } from '@angular/material';
import { MatProgressBarModule } from '@angular/material';

// Popups & modals
import { MatTooltipModule } from '@angular/material';

// Data table
import { MatPaginatorModule, MatSortModule, MatTableModule } from "@angular/material";

@NgModule({
    imports: [
        CommonModule, 
        MatCardModule, 
        MatInputModule, 
        MatSelectModule, 
        MatDatepickerModule, MatNativeDateModule,
        BrowserAnimationsModule, 
        MatRadioModule, 
        MatChipsModule, 
        MatCheckboxModule, 
        MatSliderModule, 
        MatSlideToggleModule, 
        MatProgressSpinnerModule, 
        MatProgressBarModule, 
        MatTooltipModule, 
        MatMenuModule, 
        MatDividerModule, 
        MatListModule, 
        MatTabsModule, 
        MatExpansionModule,
        MatPaginatorModule, MatSortModule, MatTableModule],
    exports: [
        CommonModule, 
        MatCardModule, 
        MatInputModule, 
        MatSelectModule, 
        MatDatepickerModule, MatNativeDateModule,
        BrowserAnimationsModule, 
        MatRadioModule, 
        MatChipsModule, 
        MatCheckboxModule, 
        MatSliderModule, 
        MatSlideToggleModule, 
        MatProgressSpinnerModule, 
        MatProgressBarModule, 
        MatTooltipModule, 
        MatMenuModule, 
        MatDividerModule, 
        MatListModule, 
        MatTabsModule, 
        MatExpansionModule,
        MatPaginatorModule, MatSortModule, MatTableModule],
    })
export class CustomMaterialModule { }