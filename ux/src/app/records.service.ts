import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subscriber } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { MessageService } from './message.service';
import { MovieRoot } from './models/movieroot';
import { Movie } from './models/movie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                             'Access-Control-Allow-Origin':'*'})
};

@Injectable({
  providedIn: 'root'
})
export class RecordsService {

  private getMoviesUri       = '/movierama/movies';         // URL to web login api
  private getLatestMoviesUri = '/movierama/latest';         // URL to web login api
  private addMovieUri        = '/movierama/add_movie';      // URL to web login api
 

  constructor(private http: HttpClient, private messageService: MessageService) { }

  getLatestMovies(): Observable<MovieRoot> {
    return this.http.get<MovieRoot>(this.getLatestMoviesUri, httpOptions).pipe(
      tap(movies => this.log('fetched latest movies')),
      catchError(this.handleError<MovieRoot>('getLatestMovies'))
    );
  }

  getLatestMoviesForCreator(creator: string): Observable<MovieRoot> {
    let moviesForCreatorUri = `${this.getLatestMoviesUri}?creator=${creator}`
    return this.http.get<MovieRoot>(moviesForCreatorUri, httpOptions).pipe(
      tap(movies => this.log('fetched latest movies for creator')),
      catchError(this.handleError<MovieRoot>('getLatestMoviesForCreator'))
    );
  }

  sortMoviesByLikes(selectedSortOrder : string): Observable<MovieRoot> {
    let sortMoviesByLikesUri = `${this.getLatestMoviesUri}?sortBy=likes&order=${selectedSortOrder};`
    return this.http.get<MovieRoot>(sortMoviesByLikesUri, httpOptions).pipe(
      tap(movies => this.log('fetched latest movies order by likes')),
      catchError(this.handleError<MovieRoot>('sortMoviesByLikes'))
    );
  }

  sortMoviesByHates(selectedSortOrder : string): Observable<MovieRoot> {
    let sortMoviesByHatesUri = `${this.getLatestMoviesUri}?sortBy=hates&order=${selectedSortOrder};`
    return this.http.get<MovieRoot>(sortMoviesByHatesUri, httpOptions).pipe(
      tap(movies => this.log('fetched latest movies order by hates')),
      catchError(this.handleError<MovieRoot>('sortMoviesByHates'))
    );
  }

  sortMoviesByDate(selectedSortOrder : string): Observable<MovieRoot> {
    let sortMoviesByDatesUri = `${this.getLatestMoviesUri}?sortBy=createdAt&order=${selectedSortOrder};`
    return this.http.get<MovieRoot>(sortMoviesByDatesUri, httpOptions).pipe(
      tap(movies => this.log('fetched latest movies order by date')),
      catchError(this.handleError<MovieRoot>('sortMoviesByDate'))
    );
  }

   /** GET movies from the server */
  getMovies(): Observable<MovieRoot> {

    // Mock Service
    // return new Observable<MovieRootElement>((subscriber: Subscriber<MovieRootElement>) => subscriber.next(new MovieRootElement()));

    return this.http.get<MovieRoot>(this.getMoviesUri, httpOptions).pipe(
      tap(movies => this.log(`fetched movies for user: ${localStorage.getItem('username')}`)),
      catchError(this.handleError<MovieRoot>('getMovies'))
    );
  }

  addMovie(movie: Movie) {

    // return new Observable<Movie>((subscriber: Subscriber<Movie>) => subscriber.next(new Movie(title, description)));
    const movTitle = movie.title;
    const movDescr = movie.description;

    return this.http.post<Movie>(this.addMovieUri, movie, httpOptions).pipe(
      tap((movie: Movie) => this.log(`added movie with title=${movTitle} and description=${movDescr}`)),
      catchError(this.handleError<any>('addMovie'))
    );
  }

  likeMovie(movie: Movie) {

    const movTitle = movie.title;
    const movDescr = movie.description;

    const id = typeof movie === 'number' ? movie : movie.id;
    const likeMovieUri = `/movierama/${id}/like`;

    return this.http.patch<Movie>(likeMovieUri, httpOptions).pipe(
      tap((movie: Movie) => this.log(`liked movie with title=${movTitle} and description=${movDescr}`)),
      catchError(this.handleError<any>('likeMovie'))
    );
  }

  hateMovie(movie: Movie) {

    const movTitle = movie.title;
    const movDescr = movie.description;

    const id = typeof movie === 'number' ? movie : movie.id;
    const hateMovieUri = `/movierama/${id}/hate`;

    return this.http.patch<Movie>(hateMovieUri, httpOptions).pipe(
      tap((movie: Movie) => this.log(`hated movie with title=${movTitle} and description=${movDescr}`)),
      catchError(this.handleError<any>('hateMovie'))
    );
  }

  retractMovie(movie: Movie) : Observable<any> {

    const movTitle = movie.title;
    const movDescr = movie.description;

    const id = typeof movie === 'number' ? movie : movie.id;
    const retractMovieUri = `/movierama/${id}/retract`;

    return this.http.patch<Movie>(retractMovieUri, movie.id, httpOptions).pipe(
      tap((movie: Movie) => this.log(`retracted movie with title=${movTitle} and description=${movDescr}`)),
      catchError(this.handleError<any>('retractMovie'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
  */ 
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
  
      let errorMsg = `error in ${operation}`
      console.error(`${errorMsg}:`, error); // log to console instead
  
      // TODO: better job of transforming error for user consumption
      if(error instanceof HttpErrorResponse) {
        errorMsg = `${operation} failed: ${error.status} - ${error.message}`;
        this.messageService.add(errorMsg);
      }
      // Let the app keep running by returning an empty result.
      // return of(result as T);
      return Observable.throw(errorMsg)
    };
  } 

   /** Log a AuthService message with the MessageService */
   private log(message: string) {
    //this.messageService.add('AuthService: ' + message);
  }
}
