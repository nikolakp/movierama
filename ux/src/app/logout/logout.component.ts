import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  csrf     = localStorage.getItem('csrf');
  username = localStorage.getItem('username');
  
  constructor(private auth: AuthService, private router: Router, private messageService: MessageService) { }

  ngOnInit() {
    this.auth.logout().subscribe(any => {
        localStorage.removeItem('username')
        localStorage.removeItem('csrf')
        this.router.navigate([''])
        this.auth.setLoggedIn(false)
    }, err => alert('Big problema on logout'));
  }
}
