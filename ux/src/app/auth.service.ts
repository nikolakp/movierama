import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable} from 'rxjs';
import { User } from './models/user';
import { tap, catchError } from 'rxjs/operators';
import { MessageService } from './message.service';
import { Csrf } from './models/csrf';

interface isLoggedin {
  status: boolean
}

interface logoutStatus {
  status: boolean
}

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 
                             'Access-Control-Allow-Origin':'*'})
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private mockRegisterUrl = 'api/heroes'; 

  private registerUri = '/movierama/register';  // URL to web register api
  private loginUri    = '/movierama/login';     // URL to web login api
  private logoutUri   = '/movierama/logout';    // URL to web login api

  private loginInStatus = false

  constructor(private http: HttpClient, private messageService: MessageService) { }

  setLoggedIn(value: boolean) {
    this.loginInStatus = value;
  }

  isLoggedIn() {
    return this.loginInStatus
  }

  checkIfLoggedIn(): Observable<isLoggedin> {
    return this.http.get<isLoggedin>('/api/isLoggedin.php')
  }

  logout() {
    // Mock Service //
    /*return this.http.get<logoutStatus>('/api/logout.php')*/
    return this.http.post(this.logoutUri, httpOptions).pipe(
      tap(_ => this.log(`log out user w/ name=${localStorage.getItem('username')}`)),
      catchError(this.handleError<any>('registerUser'))
    );
  }

  /** POST: register a new user to the server */
  registerUser (user: User): Observable<User> {
    // Mock Service //
    // return new Observable<User>((subscriber: Subscriber<User>) => subscriber.next(new User(user.username, user.password)));
    return this.http.post<User>(this.registerUri, user, httpOptions).pipe(
      tap(_ => this.log(`register user w/ name=${user.username}`)),
      catchError(this.handleError<any>('registerUser'))
    );
  }

  loginUser(user: User): Observable<Csrf> {
    // Mock Service //
    /* return this.http.post<Csrf>('/api/auth.php', {username,password}) */
    return this.http.post<Csrf>(this.loginUri, user, httpOptions).pipe(
      tap((user: User) => this.log(`logged in user w/ name=${user.username}`)),
      catchError(this.handleError<any>('loginUser'))
    );
  }

    /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
  */ 
 private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    let errorMsg = `error in ${operation}`
    console.error(`${errorMsg}:`, error); // log to console instead

    // TODO: better job of transforming error for user consumption
    if(error instanceof HttpErrorResponse) {
      errorMsg = `${operation} failed: ${error.status} - ${error.message}`;
      this.messageService.add(errorMsg);
    }
    // Let the app keep running by returning an empty result.
    // return of(result as T);
    return Observable.throw(errorMsg)
  };
} 

  /** Log a AuthService message with the MessageService */
  private log(message: string) {
    this.messageService.add('AuthService: ' + message);
  }
}
