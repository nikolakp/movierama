import { Component, OnInit } from '@angular/core';
import { MessageService } from '../message.service';
import { RecordsService } from '../records.service';
import { Movie } from '../models/movie';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  username: string;
  csrf: string;
  success = false;
  sortOrders = ['desc', 'asc'];
  selectedSortOrder = 'desc';
  movies: Movie[];

  constructor(private recordsService : RecordsService, private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.clear()
    this.username = localStorage.getItem('username');
    this.csrf = localStorage.getItem('csrf');
    this.success = !(this.username === null || this.csrf === null);

    this.recordsService.getLatestMovies().subscribe(data => {
      this.movies = data.movies;
    }, err => {
      alert('Big problem on retrieving latest movies for non-authenticated guest')
    });
  }

  filterMoviesByCreator(creator: string) {
    if(!creator) {
      this.messageService.add('passing creator value is missing!')
    }
    this.recordsService.getLatestMoviesForCreator(creator).subscribe(data => {
      this.movies = data.movies;
    }, err => {
      alert('Big problem on filtering by creator the latest movies for non-authenticated guest')
    });
  }

  sortMoviesByLikes() {
    alert(this.selectedSortOrder);
    this.recordsService.sortMoviesByLikes(this.selectedSortOrder).subscribe(data => {
      this.movies = data.movies;
    }, err => {
      alert('Big problem on filtering by likes the latest movies for non-authenticated guest')
    });
  }

  sortMoviesByHates() {
    alert(this.selectedSortOrder);
    this.recordsService.sortMoviesByHates(this.selectedSortOrder).subscribe(data => {
      this.movies = data.movies;
    }, err => {
      alert('Big problem on filtering by hates the latest movies for non-authenticated guest')
    });
  }

  sortMoviesByDate() {
    alert(this.selectedSortOrder);
    this.recordsService.sortMoviesByDate(this.selectedSortOrder).subscribe(data => {
      this.movies = data.movies;
    }, err => {
      alert('Big problem on filtering by date the latest movies for non-authenticated guest')
    });
  }

  setSelectedSortOrder(selectedSortOrder : string) {
    alert(selectedSortOrder)
    this.selectedSortOrder = selectedSortOrder;
  }
}
