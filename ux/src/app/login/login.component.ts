import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router'
import { User } from '../models/user';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService, 
              private router: Router,
              private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.clear();
  }

  loginUser(event) {
    event.preventDefault()
    const target = event.target
    const username = target.querySelector('#username').value
    const password = target.querySelector('#password').value

    this.auth.loginUser({username, password} as User).subscribe(data => {
      console.log(data)
      if (!data || !data.csrf) {
          this.messageService.add(`system cannot recognized user with name=${username}`)
          return;
      } 
      localStorage.setItem('csrf', JSON.stringify(data.csrf))
      localStorage.setItem('username', username)
      this.router.navigate(['']) 
      this.auth.setLoggedIn(true)
    })
  }
}
