import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatSortable, MatTableDataSource} from '@angular/material';
import { RecordsService} from '../records.service';
import { MessageService } from '../message.service';
import { Movie } from '../models/movie';

/**
 * @title Table with filtering
 */
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  displayedColumns: string[] = ['title', 'description', 'creator', 'createdAt', 'hates', 'likes', 'actions'];
  dataSource: MatTableDataSource<Movie>;
  highlightedRows = [];

  message = '';
  currentPage = 1;
  hasNextPage = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get username(): any {
    return localStorage.getItem('username').trim();
  }

  constructor(private recordsService : RecordsService, private messageService: MessageService) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit() {
    this.message = "Welcome ".concat(localStorage.getItem('username'));
    this.getMovies();
  }

  private getMovies() {
    this.recordsService.getMovies().subscribe(data => {

      data.movies.forEach(function(part, index, theArray) {
        part.creator = part.creator.trim()
        theArray[index] = part
      });

      this.currentPage = data.currentPage;
      this.hasNextPage = data.hasNextPage;
      this.dataSource = new MatTableDataSource<Movie>(data.movies);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }, err => {
      alert('Big problema on retrieving registered user home page');
    });
  }

  addMovie(title: string, description: string) {
    if(!title) {
      return;
    }
    if(!description) {
      this.messageService.add('try adding movie: ' + title + ' without description!')
    }
    this.recordsService
        .addMovie({ title, description } as Movie)
        .subscribe(movie => {
          this.messageService.add(`successfully added movie with title=${title} and description=${description}`)
      }, err => alert('Big problem on adding a Movie'));
  }

  likeMovie(movie: Movie): void {
    const movTitle = movie.title;
    const movDescr = movie.description;
    this.recordsService
        .likeMovie(movie)
        .subscribe(any => {
                  this.messageService.add(`successfully liked movie with title=${movTitle} and description=${movDescr}`)
                  this.getMovies()
        }, err => alert('Big problem on liking a Movie'));
  }

  hateMovie(movie: Movie): void {
    const movTitle = movie.title;
    const movDescr = movie.description;
    this.recordsService
    .hateMovie(movie)
    .subscribe(any => {
              this.messageService.add(`successfully hated movie with title=${movTitle} and description=${movDescr}`)
              movie
    }, err => alert('Big problem on hating a Movie'));
  }

  retractMovie(movie: Movie): void {
    const movTitle = movie.title;
    const movDescr = movie.description;
    this.recordsService
    .retractMovie(movie)
    .subscribe(any => {
              this.messageService.add(`successfully retracted movie with title=${movTitle} and description=${movDescr}`)
              this.getMovies()
    }, err => alert('Big problem on retracting a Movie'));
  }
}