import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RecordsService } from './records.service';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth.guard';
import { LogoutComponent } from './logout/logout.component';
import { MessagesComponent } from './messages/messages.component';
import { TableComponent } from './table/table.component';
import { CustomMaterialModule } from "./material.module";
import { RegisterComponent } from './register/register.component';
import { SearchComponent } from './search/search.component';
import { CsrfInterceptor } from './http-interceptors/csrf-interceptor';
import { MomentModule } from 'angular2-moment';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LogoutComponent,
    MessagesComponent,
    TableComponent,
    RegisterComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CustomMaterialModule,
    HttpClientModule,
    MomentModule,
    RouterModule.forRoot ([
      {
        path: 'login', //localhost:4200/login
        component: LoginComponent
      },
      {
        path: 'table', //localhost:4200/table
        component: TableComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'logout', //localhost:4200/logout
        component: LogoutComponent
      },
      {
        path: 'register', //localhost:4200/register
        component: RegisterComponent
      },
      {
        path: 'search', //localhost:4200/search
        component: SearchComponent,
        canActivate: [AuthGuard]
      },
      {
        path: '',
        component: HomeComponent
      }])
  ],
  providers: [RecordsService, AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: CsrfInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
