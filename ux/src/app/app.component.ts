import { Component } from '@angular/core';


function logc(className) {
  console.log(className)
  return (...args) => {
    console.log("Arguments passed to this class's constructor are ", args)
    return new className(...args)
  }
}

function log(target, name, descriptor) {
  console.log(target);
  console.log('*****');
  console.log(name);
  console.log('*****');
  console.log(descriptor);
  //store the original function in a variable
  const original = descriptor.value
  //do some manipulation with descriptor
  descriptor.value = function(...args) {
    console.log("Arguments ", args, " were passed in this function")
    console.log(this)
    const result = original.apply(this, args)
    console.log("The result of the function is ", result)
    return result
  }
  //return the descriptor
  return descriptor
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'movierama';
  myVariable = 'var';
  myDisabledValue = false;
  data = []
  
  ngOnInit() {
  
  }

  constructor() {
  }

  callMyFunction() {
     console.log("Function called!") 
     this.myDisabledValue = !this.myDisabledValue;
  }
}


