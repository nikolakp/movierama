<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

session_start();

unset($_SESSION);

session_destroy();

?>
{
	"status": true
}