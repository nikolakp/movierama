<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

session_start();

$user = $_SESSION['user'];

if($user == 'panos') {
    echo '{
	"currentPage": 1,
	"hasNextPage": true,
	"success": true,
	"message": "You have many rated movies!",
	"movies": [{
			"position": 1,
			"title": "America",
			"description": "Troy War",
			"creator": "nikolakp@gmail.com",
			"createdAt": "01 - 01 - 2016",
			"hates": 67,
			"likes": 7800
		},
		{
			"position": 2,
			"title": "Gladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "12 - 11 - 1988",
			"hates": 1,
			"likes": 546
		},
		{
			"position": 3,
			"title": "Bladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "12 - 11 - 1988",
			"hates": 1,
			"likes": 5
		},
		{
			"position": 4,
			"title": "Zladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "15 - 12 - 1908",
			"hates": 1,
			"likes": 53
		},
		{
			"position": 5,
			"title": "Jladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "15 - 12 - 1998",
			"hates": 1,
			"likes": 72222228
		},
		{
			"position": 6,
			"title": "Dladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "15 - 12 - 1908",
			"hates": 1,
			"likes": 34
		},
		{
			"position": 7,
			"title": "Gladiator",
			"description": "Gladiator Rules",
			"creator": "nikolakp@gmail.com",
			"createdAt": "01 - 09 - 2017",
			"hates": 1,
			"likes": 34
		}
	]
}';
} else if ($user == 'chris'){
  echo '{
	"currentPage": 1,
	"hasNextPage": true,
	"success": true,
	"message": "You have many rated movies!",
	"movies": [{
			"position": 1,
			"title": "America",
			"description": "Troy War",
			"creator": "ckaratza@gmail.com",
			"createdAt": "01 - 01 - 2016",
			"hates": 67,
			"likes": 7800
		},
		{
			"position": 2,
			"title": "Gladiator",
			"description": "Gladiator Rules",
			"creator": "ckaratza@gmail.com",
			"createdAt": "12 - 11 - 1988",
			"hates": 1,
			"likes": 546
		},
		{
			"position": 3,
			"title": "Bladiator",
			"description": "Gladiator Rules",
			"creator": "ckaratza@gmail.com",
			"createdAt": "12 - 11 - 1988",
			"hates": 1,
			"likes": 5
		},
		{
			"position": 4,
			"title": "Zladiator",
			"description": "Gladiator Rules",
			"creator": "ckaratza@gmail.com",
			"createdAt": "15 - 12 - 1908",
			"hates": 1,
			"likes": 53
		}
	]
}';
} else {
  echo '{
	"currentPage": 0,
    "hasNextPage": false,
    "success": false,
    "message": "The system cannot recognise you!",
	"movies": []
}';
}
?>
