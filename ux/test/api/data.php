<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

session_start();

$user = $_SESSION['user'];

if($user == 'panos') {
    echo '{
	"success": true,
	"message": "Successful. Welcome!",
	"movies": [{
		"title": "The Graduate",
		"year": "1967",
		"rated": "Approved"
	}, {
		"title": "Gladiator",
		"year": "1999",
		"rated": "Best-seller"
	},{
		"title": "The X-Man",
		"year": "2004",
		"rated": "Best-seller"
	},{
		"title": "Somewhere in Time",
		"year": "2009",
		"rated": "Best-seller"
	}]
}';
} else if ($user == 'chris'){
  echo '{
	"success": true,
	"message": "Successful. Welcome!",
	"movies": [{
		"title": "Troy",
		"year": "2003",
		"rated": "Approved"
	},{
		"title": "American History",
		"year": "1999",
		"rated": "Best-seller"
	},{
		"title": "The godfather",
		"year": "1980",
		"rated": "Best-seller"
	}]
}';
} else {
  echo '{
	"success": false,
	"message": "You should authenticate first!",
	"movies": [{}]
}';
}
?>
