#Movierama repository is licensed under the
#GNU General Public License v3.0

# movierama

This repository is an implementation of workable's MovieRama assignment for Software Engineer vacancy. The applications uses Datomic as persistence framework with Postgres as storage.
Besides movierama-registry a materialised-view-feeder component is included which demonstrates the mechanism of subscribing to the datomic transaction log, in order to create customised materialised views
depending on the needs (which in our case is the depiction of hates/likes for each movie and the addition of a new movie). This micro-service feeds an elastic-search instance which can be used for various movie queries.
The feed of elastic-search is being done via a mechanism that queries all transactions that have performed a hate/like/retract or add new movie event to a specific movie and updates elastic-search accordingly.

At the moment we are using Datomic Starter license option (http://www.datomic.com/get-datomic.html).

## Instructions for back-end

Necessary steps before starting:

Install Datomic jars in local maven repository (<root> is your local root directory of the project)
```cd <root>/docker/datomic/lib```
```mvn install:install-file -DgroupId=com.datomic -DartifactId=datomic-pro -Dfile=<root>/docker/datomic/lib/datomic-pro-0.9.5561.50.jar -DpomFile=pom.xml.datomic```
```mvn install:install-file -DgroupId=com.datomic -DartifactId=datomic-transactor-pro -Dfile=<root>/docker/datomic/lib/datomic-transactor-pro-0.9.5561.50.jar -DpomFile=pom.xml.datomic```

To build the project just:
```cd <root>```
```mvn clean install -DskipTests=true```

In order to run both movierama-registry and materialised-view-feeder locally for development purposes you need to:
Change the <root>/docker/datomic/sql-transactor.properties host from datomic to host=0.0.0.0

Run datomic transactor with postgres and elasticsearch just:
```cd <root>/docker```
```sudo docker-compose -f datomic-postgres-elasticsearch.yml up --build```

Run the Application.java from your favorite IDE for both modules with program startup parameters

```1. -c <root>/configuration/movierama-registry -b registry-module-beans.xml```

and the materialiser with jvm parameters:
```2. -javaagent:<root>/configuration/quasar/quasar-core-0.7.7.jar -DappConfig=<root>/configuration/materialised-view-feeder/```

For all docker image builds multistage build is used (https://docs.docker.com/develop/develop-images/multistage-build/)

To build the docker image for the movierama-registry only just:
```cd <root>```
```docker build -f Dockerfile_movierama_registry -t movierama-registry .```

To build the docker image for the materialised-view-feeder only just:
```cd <root>```
```docker build -f Dockerfile_materialised_view_feeder -t materialised-view-feeder .```

To run everything including all dependencies (datomic, postgres, elastic-search, movierama-registry, materialised-view-feeder) just:

Change the <root>/docker/datomic/sql-transactor.properties host to host=datomic

```cd <root>/docker```
```docker-compose -f runAll.yml up --build```

The OpenApi spec can be accessed at https://0.0.0.0:8900/movierama/spec.yaml
From there you can browse all operations by copy-pasting the file in http://editor.swagger.io/

If elestic search has virtual memory problems use sudo sysctl -w vm.max_map_count=262144 in linux.

## Instructions for front-end based on Angular 6

Install Node.js and npm (https://nodejs.org/en/download/) if they are not already on your machine.

Verify that you are running at least Node.js version 8.x or greater and npm version 5.x or greater by running node -v and npm -v in a terminal/console window. 
Older versions produce errors, but newer versions are fine.

Install Modules with npm install
```cd <root>/ux```
```npm install```

Serve the application
Go to the project directory and launch the server by executing ng start.
```cd <root>/ux```
```npm start```
Using the browser on http://localhost:4200/ to navigate to the GUI of movierama.
