#!/bin/bash

APPLICATION=workable.movierama.feeder

APPLICATION_HOME=$( cd $(dirname $0)/.. && pwd )
APPLICATION_CFG_DIR=$APPLICATION_HOME/config
APPLICATION_LOGS=$APPLICATION_HOME/logs

JVM_ARGS="-da -Xms${APPLICATION_MEM:-1G} -XX:+UseConcMarkSweepGC \
 -Dfile.encoding=UTF-8 \
 -Dcom.sun.management.jmxremote.port=1901\
 -Dcom.sun.management.jmxremote.authenticate=false\
 -Dcom.sun.management.jmxremote.ssl=false"


JVM_ARGS="$JVM_ARGS\
 -Dlogback.configurationFile=${APPLICATION_CFG_DIR}/logback.xml"

MAIN_CLASS=com.$APPLICATION.application.Application

cd $APPLICATION_HOME

run_me() {
    exec java -javaagent:$APPLICATION_HOME/lib/quasar-core-0.7.7.jar -cp "$APPLICATION_HOME/lib/*" $JVM_ARGS -DLOG_PATH=$APPLICATION_LOGS/ -Dco.paralleluniverse.fibers.detectRunawayFibers=false -DappConfig=$APPLICATION_CFG_DIR/ $MAIN_CLASS
}
