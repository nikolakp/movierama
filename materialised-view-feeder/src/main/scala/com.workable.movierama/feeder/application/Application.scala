package com.workable.movierama.feeder.application

import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.datomic.DatomicSchemaOps.setUp
import com.workable.movierama.feeder.services.CollectorService._
import com.workable.movierama.feeder.services.ElasticSearchService

import scala.util.Random

/** @author nikolakp
  *         The entry point for the elasticsearch feeder. Starts the collector service
  *         from a designated endpoint with the option to start from the beginning and reset the materialised view.
  *
  */

object Application extends App with LazyLogging {

  implicit val rand: Random = new Random()
  setUp()
  logger.info("ElasticSearch client initialised {}", ElasticSearchService.client)
  startCollecting()
}
