package com.workable.movierama.feeder.util

import java.io.File

import com.typesafe.config.{Config, ConfigFactory}

/** @author nikolap
 */

object ConfigBuilder {
  val CONFIG_PROP = "appConfig"
}

/** A config trait. Uses TypeSafe JVM config library that is based on Hocon format. If a system variable (appConfig) is specified it attempts
 *  to load all the resources specified in the configList from that path. If no system variable present, it tries to load them
 *  from the classpath. Next step is to make the library consul-aware and provide a 3rd option to load configuration in a transparent way.
 */
trait ConfigBuilder {

  import ConfigBuilder.CONFIG_PROP

  implicit lazy val config: Config = buildConfig()

  val configPath = Option(System.getProperty(CONFIG_PROP))

  def configList: Array[String]

  protected def buildConfig(): Config = {
    Option(System.getProperty(CONFIG_PROP)) map { configPath ⇒
      configList map (configName ⇒
        ConfigFactory
          .parseFile(new File(configPath + configName))) reduceLeft ((left: Config,
        right: Config) ⇒
        left.withFallback(right)) resolve ()
    } getOrElse (configList map (configName ⇒
      ConfigFactory.load(configName)) reduceLeft ((left: Config,
      right: Config) ⇒
      left.withFallback(right)) resolve ())
  }
}
