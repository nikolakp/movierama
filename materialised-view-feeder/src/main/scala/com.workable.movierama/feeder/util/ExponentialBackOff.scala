package com.workable.movierama.feeder.util

import scala.concurrent.duration.{ Duration, FiniteDuration }
import scala.util.Random

/** @author nikolakp
 *         An exponential backoff mechanism to be used by collector service.
 */

case class ExponentialBackOff(slotTime: FiniteDuration, ceiling: Int = 10,
                              slot: Int = 1, waitTime: FiniteDuration = Duration.Zero,
                              retries: Int = 0, resets: Int = 0, totalRetries: Long = 0)(implicit rand: Random) {
  def isStarted: Boolean = retries > 0

  def nextBackOff: ExponentialBackOff = {
    def time: FiniteDuration = slotTime * times

    def times = {
      val exp = rand.nextInt(slot + 1)
      math.round(math.pow(2, exp) - 1)
    }

    if (slot >= ceiling) reset()
    else {
      val (newSlot, newWait: FiniteDuration) = if (slot >= ceiling) {
        (ceiling, time)
      }
      else {
        (slot + 1, time)
      }
      copy(slot = newSlot,
        waitTime = newWait,
        retries = retries + 1,
        totalRetries = totalRetries + 1)
    }
  }

  def reset(): ExponentialBackOff = {
    copy(slot = 1, waitTime = Duration.Zero, resets = resets + 1, retries = 0)
  }
}