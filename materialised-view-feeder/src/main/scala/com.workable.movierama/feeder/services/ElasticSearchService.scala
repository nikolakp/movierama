package com.workable.movierama.feeder.services

import java.util

import com.sksamuel.elastic4s.ElasticsearchClientUri
import com.sksamuel.elastic4s.http.HttpClient
import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.datomic.ORM.TxData
import com.workable.movierama.feeder.util.ConfigBuilder
import org.elasticsearch.action.support.WriteRequest.RefreshPolicy

/** @author nikolakp
  *         ElasticSearch Service that upserts documents based on input.
  *
  */
object ElasticSearchService extends ConfigBuilder with LazyLogging {

  import com.sksamuel.elastic4s.http.ElasticDsl._

  import scala.collection.JavaConverters._

  val client = HttpClient(ElasticsearchClientUri(config.getString("elasticsearch.host"), config.getInt("elasticsearch.port")))

  def indexDocumentList(batchList: util.List[(TxData, Long)]) = {
    logger.info("Trying to index list with size {}", batchList.size())
    val list = batchList.asScala.filter(p => p._1.op.isEmpty)
    if (list.nonEmpty) {
      client.execute {
        bulk(list map { x ⇒
          logger.info("Incoming new message {}", x)
          x._1.movieCoreInfo match {
            case Some(movieCoreInfo) =>
              indexInto("movierama" / "movies") id x._1.uuid fields
                ("id" -> x._1.uuid,
                  "title" -> movieCoreInfo.title,
                  "description" -> movieCoreInfo.description,
                  "creator" -> movieCoreInfo.creator,
                  "createdAt" -> movieCoreInfo.createdAt,
                  "hates" -> 0,
                  "likes" -> 0
                )
          }
        }).refresh(RefreshPolicy.WAIT_UNTIL)
      }.await
    }

    val opList = batchList.asScala.filter(p => p._1.op.isDefined)
    if (opList.nonEmpty) {
      client.execute {
        bulk(opList map { x ⇒
          logger.info("Incoming new message {}", x)
          x._1.op match {
            case Some(op) =>
              val trasition = op
              trasition match {
                case "+h-l" => {
                  update(x._1.uuid).in("movierama" / "movies") script "ctx._source.hates+=1"
                  update(x._1.uuid).in("movierama" / "movies") script "ctx._source.likes-=1"
                }
                case "+l-h" => {
                  update(x._1.uuid).in("movierama" / "movies") script "ctx._source.hates-=1"
                  update(x._1.uuid).in("movierama" / "movies") script "ctx._source.likes+=1"
                }
                case "-h" => update(x._1.uuid).in("movierama" / "movies") script "ctx._source.hates-=1"
                case "-l" => update(x._1.uuid).in("movierama" / "movies") script "ctx._source.likes-=1"
                case "+h" => update(x._1.uuid).in("movierama" / "movies") script "ctx._source.hates+=1"
                case "+l" => update(x._1.uuid).in("movierama" / "movies") script "ctx._source.likes+=1"
                case _ => update(x._1.uuid).in("movierama" / "movies") script "ctx._source.likes+=0"
              }
          }
        }).refresh(RefreshPolicy.WAIT_UNTIL)
      }.await
    }
  }

  override def configList: Array[String] = Array("elasticsearch.conf")
}
