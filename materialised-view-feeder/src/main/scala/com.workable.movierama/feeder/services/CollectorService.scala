package com.workable.movierama.feeder.services

import java.util.concurrent.{LinkedBlockingQueue, TimeUnit}
import java.{lang, util}

import clojure.lang.PersistentVector
import co.paralleluniverse.fibers.Fiber
import co.paralleluniverse.strands.SuspendableRunnable
import co.paralleluniverse.strands.channels.{Channel, Channels}
import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.datomic.DatomicAdapter._
import com.workable.movierama.feeder.datomic.ORM._
import com.workable.movierama.feeder.datomic.QueryLoader._
import com.workable.movierama.feeder.services.CheckpointService._
import com.workable.movierama.feeder.services.ElasticSearchService.indexDocumentList
import com.workable.movierama.feeder.util.{ConfigBuilder, ExponentialBackOff}

import scala.concurrent.duration.FiniteDuration
import scala.util.Random

/** @author nikolakp
  *         The service that polls for specific txs that are of interest for the materialized view and  delegates to the appropriate
  *         service to build the view.
  */
object CollectorService extends ConfigBuilder with LazyLogging {

  implicit val rand: Random = new Random()
  val resetCheckPoint: Boolean = config.getBoolean("feeder.resetCheckpoint")
  val batchSize: Int = config.getInt("feeder.batchSize")
  val slotTime: Int = config.getInt("feeder.slotTime")
  val checkpointChannel: Channel[Long] = Channels.newChannel[Long](10)
  val processorChannel: Channel[Boolean] = Channels.newChannel[Boolean](1)
  val processorDataStructures: ProcessorDataStructures = ProcessorDataStructures()

  def startCollecting(): Unit = {
    processorDataStructures.clear()
    startPollingProcess()
    startProcessor(processorDataStructures)
    startCheckpointChecker()
  }

  def startPollingProcess(): Fiber[Unit] = {
    val fiber = new Fiber[Unit]("pollFromDatomic", new SuspendableRunnable() {
      override def run(): Unit = {
        var pollingBackOff = ExponentialBackOff(slotTime = FiniteDuration.apply(slotTime, TimeUnit.MILLISECONDS))
        var checkpoint: Long = if (resetCheckPoint) -1 else getLastCommittedCheckpoint
        logger.debug("Current Checkpoint is {}", checkpoint)
        while (true) {
          val pollMaxTx = try {
            poll(checkpoint)
          }
          catch {
            case t: Throwable =>
              logger.error("Exception encountered  {}", t)
              -1
          }
          logger.debug("Polled from {}. New potential checkpoint is {}", s"$checkpoint", s"$pollMaxTx")
          if (pollMaxTx == -1) {
            pollingBackOff = pollingBackOff.nextBackOff
            Thread.sleep(pollingBackOff.waitTime.length)
          }
          else {
            checkpoint = pollMaxTx
            processorChannel.send(true)
            pollingBackOff = pollingBackOff.reset()
          }
        }
      }
    })
    fiber.start()
    fiber
  }

  def poll(checkpoint: Long): Long = {
    val result = dquery(query("txs"), new lang.Long(checkpoint)).asInstanceOf[PersistentVector]
    result.forEach(pv ⇒ processorDataStructures.eventMessageQueue.add(pv.asInstanceOf[PersistentVector]))
    if (result == null || result.size() == 0) -1
    else result.get(result.size() - 1).asInstanceOf[PersistentVector].get(1).asInstanceOf[Long]
  }

  def startProcessor(processorDataStructures: ProcessorDataStructures): Fiber[Unit] = {
    val eventMessageQueue = processorDataStructures.eventMessageQueue
    val activeBatchList = processorDataStructures.activeBatchList

    def processNextBatch(): Boolean = {
      def processActiveBatchList(): Boolean = {
        if (!activeBatchList.isEmpty) {
          logger.debug("Trying to index to elasticsearch")
          try {
            indexDocumentList(activeBatchList)
            checkpointChannel.send(activeBatchList.get(activeBatchList.size() - 1)._2)
            activeBatchList.clear()
            true
          }
          catch {
            case t: Throwable =>
              logger.error("Exception encountered  {}", t)
              println(t)
              false
          }
        }
        else false
      }

      while (eventMessageQueue.peek() != null && activeBatchList.size() < batchSize) {
        Option(eventMessageQueue.poll()) map {
          tx ⇒ loadEntityFromQueryResult(tx) map (appRev ⇒ activeBatchList.add((appRev._1, tx.get(1).asInstanceOf[Long])))
        }
      }
      processActiveBatchList()
    }

    def createFiber(): Fiber[Unit] = {
      new Fiber[Unit]("indexToElasticSearch", new SuspendableRunnable() {
        override def run(): Unit = {
          var backOff = ExponentialBackOff(slotTime = FiniteDuration.apply(slotTime, TimeUnit.MILLISECONDS))
          while (true) {
            processorChannel.receive(backOff.waitTime.length, TimeUnit.MILLISECONDS)
            if (!processNextBatch()) backOff = backOff.nextBackOff
            else backOff.reset()
          }
        }
      })
    }

    val fiber = createFiber()
    fiber.start()
    fiber
  }

  def startCheckpointChecker(): Fiber[Unit] = {
    new Fiber[Unit]("checkpointChecker", new SuspendableRunnable() {
      override def run(): Unit = {
        while (true) {
          val checkpoint = checkpointChannel.receive()
          try {
            commitCheckpoint(checkpoint)
            logger.info("Committed checkpoint {}", checkpoint)
          }
          catch {
            case t: Throwable =>
              logger.error("Exception encountered  {}", t)
          }
        }
      }
    }).start()
  }

  override def configList: Array[String] = Array("application.conf")

  case class ProcessorDataStructures(eventMessageQueue: LinkedBlockingQueue[PersistentVector] = new LinkedBlockingQueue[PersistentVector](),
                                     activeBatchList: util.List[(TxData, Long)] = new util.ArrayList[(TxData, Long)](batchSize)) {
    def clear(): Unit = {
      eventMessageQueue.clear()
      activeBatchList.clear()
    }
  }

}
