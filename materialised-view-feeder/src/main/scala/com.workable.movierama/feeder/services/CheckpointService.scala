package com.workable.movierama.feeder.services

import java.lang

import clojure.lang.PersistentVector
import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.datomic.QueryLoader._
import com.workable.movierama.feeder.datomic.DatomicAdapter._
import com.workable.movierama.feeder.datomic.ORM._
import com.workable.movierama.feeder.util.ConfigBuilder;

/** @author nikolakp
 *         Commit and retrieval of feeder checkpoint.
 */
object CheckpointService extends ConfigBuilder with LazyLogging {

  private val feederId = config.getString("feeder.feederId")

  def resetColdStartCheckpoint(): java.util.Map[_, _] = {
    commitCheckpoint(-1)
  }

  def commitCheckpoint(checkpoint: Long): java.util.Map[_, _] = {
    import scala.collection.JavaConverters._
    val obj = dquery(query("checkpointQuery"), feederId).asInstanceOf[PersistentVector]
    val idMap = extractEntityIdsFromQueryResult(obj) map {
      ids ⇒ Map(":db/id" -> ids.head)
    } getOrElse Map.empty
    val inTxDs = List((idMap ++ Map(":movieRegistry/feederId" -> feederId, ":movieRegistry/esCheckpoint" -> new lang.Long(checkpoint))).asJava).asJava
    val tx = transact(inTxDs).get()
    println(tx)
    tx
  }

  def getLastCommittedCheckpoint: Long = {
    val obj = dquery(query("checkpointQuery"), feederId).asInstanceOf[PersistentVector]
    loadIndexFromSingleQuery[Long](obj, 1).getOrElse(-1)
  }

  override def configList: Array[String] = Array("application.conf")
}
