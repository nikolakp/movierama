package com.workable.movierama.feeder.datomic

import java.util

import datomic.{Connection, ListenableFuture, Peer}

/** @author nikolakp
 *         Abstracts Datomic's Query and transact operations in order to decouple from actual service implementations.
 */
object DatomicAdapter {

  /** Java interop issue with varags we have to overload explicitly... */
  def dquery(query: AnyRef)(implicit connection: Connection): Any = Peer.query(query, connection.db())

  def dquery(query: AnyRef, x1: AnyRef)(implicit connection: Connection): Any = Peer.query(query, connection.db(), x1)

  def dquery(query: AnyRef, x1: AnyRef, x2: AnyRef)(implicit connection: Connection): Any = Peer.query(query, connection.db(), x1, x2)

  def dquery(query: AnyRef, x1: AnyRef, x2: AnyRef, x3: AnyRef)(implicit connection: Connection): Any = Peer.query(query, connection.db(), x1, x2, x3)

  def dqueryFromHistory(query: AnyRef, x1: AnyRef, x2: AnyRef, x3: AnyRef)(implicit connection: Connection): Any = Peer.query(query, connection.db().history(), x1, x2, x3)

  def transact(list: java.util.List[_])(implicit connection: Connection): ListenableFuture[util.Map[_, _]] = connection.transact(list)
}
