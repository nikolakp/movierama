package com.workable.movierama.feeder.datomic

import clojure.lang.PersistentVector
import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.util.ConfigBuilder
import datomic.{Connection, Peer}

/**
  * @author nikolap
  *
  */
object ORM extends ConfigBuilder with LazyLogging {

  implicit val connection: Connection = Peer.connect(config.getString("datomic.connectionString"))

  def loadEntityFromQueryResult(queryResult: PersistentVector): Option[(TxData, Long)] = {
    /*Always extract entity from zero index*/
    if (queryResult != null && queryResult.size() > 0) {
      val db = connection.db()
      val entityId = queryResult.get(0).asInstanceOf[java.lang.Long]
      val entity = db.entity(entityId)
      if (entity.get(":movieRegistry/action") != null) {
        Some((TxData(entity.get(":movieRegistry/movieId").asInstanceOf[String],
          Some(entity.get(":movieRegistry/transition").asInstanceOf[String]), None), entityId))
      }
      else {
        Some((TxData(entity.get(":movieRegistry/id").asInstanceOf[String],
          None, Some(MovieCoreInfo(entity.get(":movieRegistry/title").asInstanceOf[String],
            entity.get(":movieRegistry/description").asInstanceOf[String], entity.get(":movieRegistry/creator").asInstanceOf[String],
            entity.get(":movieRegistry/createdAt").asInstanceOf[Long]))), entityId))
      }
    }
    else None
  }

  def extractEntityIdsFromQueryResult(queryResult: PersistentVector): Option[List[Long]] = {
    if (queryResult != null && queryResult.size() > 0) {
      Some(queryResult.toArray map {
        v => v.asInstanceOf[Long]
      } toList)
    }
    else None
  }

  def loadIndexFromSingleQuery[T](queryResult: PersistentVector, index: Int): Option[T] = {
    if (queryResult != null && queryResult.size() > index) {
      Some(queryResult.get(index).asInstanceOf[T])
    } else None
  }

  override def configList: Array[String] = Array("datomic.conf")

  case class TxData(uuid: String, op: Option[String], movieCoreInfo: Option[MovieCoreInfo])

  case class MovieCoreInfo(title: String, description: String, creator: String, createdAt: Long)

}
