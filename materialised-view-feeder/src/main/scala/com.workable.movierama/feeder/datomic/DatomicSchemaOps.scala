package com.workable.movierama.feeder.datomic

import java.io.FileReader

import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.util.ConfigBuilder
import datomic.{Connection, Peer, Util}

/**
  * @author nikolakp
  *         Installs Datomic schema loaded from edn definition file and also installs composite constraint database function.
  */

object DatomicSchemaOps extends ConfigBuilder with LazyLogging {

  override def configList: Array[String] = Array("datomic.conf")

  def setUp(dbFunctionsSetUp: (Connection) => Unit = (connection: Connection) => ()): Unit = {
    val connectionString = config.getString("datomic.connectionString")
    val schemaPath = configPath.get
    if (!schemaPath.isEmpty) {
      if (Peer.createDatabase(connectionString)) logger.info("DB successfully created")
      val connection = Peer.connect(connectionString)
      val schemaReader = new FileReader(s"$schemaPath/${config.getString("datomic.schemaFile")}")
      val schemaTx = Util.readAll(schemaReader).get(0).asInstanceOf[java.util.List[Any]]
      val txResult = connection.transact(schemaTx).get()
      println("Schema Creation Transaction Result {}", txResult)
      dbFunctionsSetUp(connection)
      connection.release()
    }
    else
      throw new IllegalArgumentException("datomic.db.creation.failure")
  }
}
