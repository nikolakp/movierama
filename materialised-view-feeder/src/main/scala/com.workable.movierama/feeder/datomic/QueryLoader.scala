package com.workable.movierama.feeder.datomic

import java.io.FileReader

import clojure.lang.{Keyword, PersistentArrayMap}
import com.typesafe.scalalogging.LazyLogging
import com.workable.movierama.feeder.util.ConfigBuilder
import datomic.Util

/**
  * @author nikolakp
  *         Loads all Datomic application-related Queries from edn definition file.
  */

object QueryLoader extends ConfigBuilder with LazyLogging {

  private val queryPath = configPath.get
  private val queryReader = new FileReader(s"$queryPath/${config.getString("datomic.queryFile")}")
  private val queryMap = Util.readAll(queryReader).get(0).asInstanceOf[PersistentArrayMap]

  override def configList: Array[String] = Array("datomic.conf")

  def query(key: String): AnyRef = {
    queryMap.get(Keyword.find(key))
  }
}
