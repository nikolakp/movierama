package com.workable.movierama.commons.util;

import io.vertx.core.Vertx;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;

/**
 * @author nikolakp
 * Adds a standard health check endpoint to a router
 */
public final class HealthCheckUtil {

    public static Router addHealthCheck(Router in, Vertx vertx) {
        HealthCheckHandler healthCheckHandler = HealthCheckHandler.create(vertx);
        in.get("/health").handler(healthCheckHandler);
        healthCheckHandler.register("OK", future -> future.complete(Status.OK()));
        return in;
    }
}
