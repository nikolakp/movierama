package com.workable.movierama.registry.application;

import com.workable.movierama.commons.SpringBootConfig;
import com.workable.movierama.commons.SpringContextUtils;
import com.workable.movierama.registry.routes.MovieRamaRoutes;
import io.vertx.core.AsyncResult;
import io.vertx.core.Vertx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Optional;

/**
 * @author nikolakp
 * The main entry point of the MovieRama Registry Application.
 */
public class Application {

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringBootConfig bootConfig = SpringBootConfig.fromCommandLineArgs(args, Optional.empty());
        System.setProperty("confDir", bootConfig.getConfigDir().getAbsolutePath());
        ApplicationContext ctx = SpringContextUtils.bootSpringApplication(bootConfig);
        MovieRamaRoutes instance = ctx.getBean(MovieRamaRoutes.class);
        Vertx.vertx().deployVerticle(instance, Application::handleDeploymentResult);
        log.info("MovieRama Verticle has started...");
    }

    private static void handleDeploymentResult(AsyncResult<String> result) {
        if (result.failed()) {
            log.error("Verticle Deployment failed: {}.", result.cause());
            System.exit(666);
        }
    }
}
