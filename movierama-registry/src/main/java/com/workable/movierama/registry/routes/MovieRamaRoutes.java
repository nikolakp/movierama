package com.workable.movierama.registry.routes;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.net.JksOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.workable.movierama.commons.util.HealthCheckUtil.addHealthCheck;

@Component
public class MovieRamaRoutes extends AbstractVerticle {

    private final String keystorePass;
    private final String keystoreFilePath;
    private final String scheme;
    private final String contextPath;
    private final String specFileName;
    private final MovieRamaHandlers movieRamaHandlers;
    private final Integer port;
    private final String host;
    private Router router;


    public MovieRamaRoutes(@Value("${keystorePass}") String keystorePass, @Value("${keystoreFilePath}") String keystoreFilePath,
                           @Value("${global.scheme}") String scheme,
                           @Value("${global.contextPath}") String contextPath,
                           @Value("${specFileName}") String specFileName, MovieRamaHandlers movieRamaHandlers,
                           @Value("${global.port}") Integer port, @Value("${global.host}") String host) {
        this.keystorePass = keystorePass;
        this.keystoreFilePath = keystoreFilePath;
        this.scheme = scheme;
        this.contextPath = contextPath;
        this.specFileName = specFileName;
        this.movieRamaHandlers = movieRamaHandlers;
        this.port = port;
        this.host = host;
    }

    public Router createRouter() {
        router = Router.router(vertx);
        router.route().handler(CorsHandler.create("*").allowedHeader("*")
                .allowedMethod(HttpMethod.GET).allowedMethod(HttpMethod.OPTIONS)
                .allowedMethod(HttpMethod.PATCH).allowedMethod(HttpMethod.POST));
        router.route().handler(BodyHandler.create());
        router.route().handler(CookieHandler.create());
        router.route().handler(getSessionHandler());

        router.route().handler(LoggerHandler.create());
        router.route().handler(ResponseTimeHandler.create());

        router.post(contextPath + "login").handler(movieRamaHandlers::handleLogin);
        router.post(contextPath + "logout").handler(movieRamaHandlers::handleLogout);
        router.post(contextPath + "register").handler(movieRamaHandlers::handleRegister);

        router.get(contextPath + "movies").handler(movieRamaHandlers::handleGet);

        router.get(contextPath + "latest").handler(movieRamaHandlers::handlePublicGet);

        router.post(contextPath + "add_movie").handler(movieRamaHandlers::handleAddMovie);

        router.patch(contextPath + ":movieID/hate").handler(movieRamaHandlers::handleHate);
        router.patch(contextPath + ":movieID/like").handler(movieRamaHandlers::handleLike);
        router.patch(contextPath + ":movieID/retract").handler(movieRamaHandlers::handleRetract);
        return router;
    }

    private SessionHandler getSessionHandler() {
        SessionHandler sessionHandler = SessionHandler.create(LocalSessionStore.create(vertx));
        sessionHandler.setSessionCookieName("JSESSIONID");
        sessionHandler.setCookieHttpOnlyFlag(true);
        return sessionHandler;
    }

    @Override
    public void start() {
        router = createRouter();
        addHealthCheck(router, vertx);
        final String configDir = System.getProperty("confDir");
        router.get(contextPath + specFileName).handler(routingContext -> {
            try {
                routingContext.response()
                        .putHeader("Content-Type", "text/plain")
                        .end(FileUtils.readFileToString(FileUtils.getFile(configDir + System.getProperty("file.separator") + specFileName), "UTF-8"));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        HttpServerOptions options = getHttpServerOptions();
        vertx.createHttpServer(options).requestHandler(router::accept).listen();
    }

    private HttpServerOptions getHttpServerOptions() {
        HttpServerOptions options = new HttpServerOptions();
        options.setHost(host);
        options.setPort(port);
        options.setSsl("https".equals(scheme));
        if (options.isSsl()) {
            options.setKeyStoreOptions(new JksOptions().setPath(keystoreFilePath).setPassword(keystorePass));
        }
        return options;
    }
}
