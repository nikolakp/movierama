package com.workable.movierama.registry.core;

import io.vertx.core.Vertx;
import io.vertx.core.json.DecodeException;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * @author nikolakp
 */
@Component
public class VertxInvocationHandler {

    private static final Logger log = LoggerFactory.getLogger(VertxInvocationHandler.class);

    public <InForService, OutFromService extends ServiceResponse<?>> void invocationPipeline(RoutingContext ctx,
                                                                                             final String apiCall,
                                                                                             VertxInputCapture<InForService> inputCapture,
                                                                                             boolean requiresAuth,
                                                                                             VertxServiceInvocation<ServiceRequest<InForService, String>,
                                                                                                     OutFromService> serviceInvocation) {
        Vertx.currentContext().owner().executeBlocking(future -> {
            if (requiresAuth) {
                Session session = ctx.session();
                String csrf = ctx.request().getHeader("CSRF");
                String id = session == null ? null : session.get("principal");
                String sCsrf = session == null ? null : session.get("csrf");
                log.warn("Authentication Info => csrf={}, id={}, sCsrf={}", csrf, id, sCsrf);
                if (csrf == null || !csrf.equals(sCsrf) || id == null) {
                    ctx.response().setStatusCode(401);
                    future.fail("UNAUTHORISED");
                    return;
                }
            }
            OutFromService outFromService;
            try {
                ServiceRequest<InForService, String> serviceRequest = new ServiceRequest<>(inputCapture.fromContext(ctx), ctx.session() == null ? null : ctx.session().get("principal"));
                outFromService = serviceInvocation.executeService(ctx, serviceRequest);
                future.complete(outFromService);
            } catch (Exception t) {
                log.error("Invocation of service failed {} for apiCall {}.", t.getMessage(), apiCall);
                if (t instanceof DecodeException)
                    ctx.response().setStatusCode(400);
                else
                    ctx.response().setStatusCode(500);
                future.fail(t);
            }
        }, outcome -> {
            Object result = outcome.result();
            if (result instanceof ServiceResponse<?> && !((ServiceResponse) result).isSuccess()) {
                serviceErrorToHttpLayerTranslator(ctx, (ServiceResponse<?>) result);
            }
            if (!ctx.response().ended() && !ctx.response().closed())
                ctx.response().end();
        });
    }

    /**
     * Here depending on the user's locale we can translate the error message...
     */
    private void serviceErrorToHttpLayerTranslator(RoutingContext ctx, ServiceResponse<?> serviceResponse) {
        if (serviceResponse.getErrorCode() == null) ctx.response().setStatusCode(500);
        else {
            switch (serviceResponse.getErrorCode()) {
                case "unique.constraint.violation":
                    ctx.response().setStatusCode(400);
                    break;
                case "existence.constraint.violation":
                    ctx.response().setStatusCode(400);
                    break;
                case "BAD.USERNAME":
                    ctx.response().setStatusCode(400);
                    break;
                case "BAD.PASSWORD":
                    ctx.response().setStatusCode(400);
                    break;
                case "USERNAME.EXISTS":
                    ctx.response().setStatusCode(400);
                    break;
                case "BAD.MOVIE.INPUT":
                    ctx.response().setStatusCode(400);
                    break;
                case "INVALID.CREDENTIALS":
                    ctx.response().setStatusCode(401);
                    break;
                case "composite.constraint.violation":
                    ctx.response().setStatusCode(403);
                    break;
                case "ENTITY.NOT.FOUND":
                    ctx.response().setStatusCode(404);
                    break;
                default:
                    ctx.response().setStatusCode(500);
                    break;
            }
            if (serviceResponse.getErrorCode() != null || serviceResponse.getErrorDescription() != null) {
                String error = Json.encode(new GenericError(serviceResponse.getErrorCode(), serviceResponse.getErrorDescription()));
                log.error("Response error {}:", error);
                ctx.response().setChunked(true);
                ctx.response().write(error);
            }
        }

    }
}
