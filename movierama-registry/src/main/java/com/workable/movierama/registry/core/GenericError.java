package com.workable.movierama.registry.core;

public class GenericError {

    private String errorCode;
    private String errorDescription;

    GenericError(String errorCode, String errorDescription) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public GenericError() {
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
