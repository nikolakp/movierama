package com.workable.movierama.registry.services;

import java.util.List;

public class Protocol {

    public static class QueryResponse {
        private long currentPage;
        private boolean hasNextPage;
        private List<Model.MovieInfo> movies;

        public QueryResponse(long currentPage, boolean hasNextPage, List<Model.MovieInfo> movies) {
            this.currentPage = currentPage;
            this.hasNextPage = hasNextPage;
            this.movies = movies;
        }

        public QueryResponse() {
        }


        public void setCurrentPage(long currentPage) {
            this.currentPage = currentPage;
        }

        public void setHasNextPage(boolean hasNextPage) {
            this.hasNextPage = hasNextPage;
        }

        public void setMovies(List<Model.MovieInfo> movies) {
            this.movies = movies;
        }

        public long getCurrentPage() {
            return currentPage;
        }

        public boolean isHasNextPage() {
            return hasNextPage;
        }

        public List<Model.MovieInfo> getMovies() {
            return movies;
        }
    }

    public static class LoginResponse {
        private String csrf;

        public LoginResponse(String csrf) {
            this.csrf = csrf;
        }

        public LoginResponse() {
        }

        public void setCsrf(String csrf) {
            this.csrf = csrf;
        }

        public String getCsrf() {
            return csrf;
        }
    }

    public static class LoginCmd {
        private String username;
        private String password;

        public LoginCmd() {
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public LoginCmd(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class RegisterCmd {
        private String username;
        private String password;

        public RegisterCmd() {
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public RegisterCmd(String username, String password) {
            this.username = username;
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }
    }

    public static class AddMovieCmd {
        private String title;
        private String description;

        public AddMovieCmd(String title, String description) {
            this.title = title;
            this.description = description;
        }

        public AddMovieCmd() {
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }
    }

    public static class ActionCmd {
        private String movieId;
        private String action;

        public ActionCmd(String movieId, String action) {
            this.movieId = movieId;
            this.action = action;
        }

        public ActionCmd() {
        }


        public void setMovieId(String movieId) {
            this.movieId = movieId;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public String getMovieId() {
            return movieId;
        }

        public String getAction() {
            return action;
        }
    }

    public static class QueryCmd {

        private String creatorId;
        private String sortBy;
        private String pageSize;
        private String offset;
        private boolean asc;

        public QueryCmd() {
        }

        public void setCreatorId(String creatorId) {
            this.creatorId = creatorId;
        }

        public void setSortBy(String sortBy) {
            this.sortBy = sortBy;
        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public void setOffset(String offset) {
            this.offset = offset;
        }

        public QueryCmd(String creatorId, String sortBy, String pageSize, String offset, boolean asc) {
            this.creatorId = creatorId;
            this.sortBy = sortBy;
            this.pageSize = pageSize;
            this.offset = offset;
            this.asc = asc;
        }

        public boolean isAsc() {
            return asc;
        }

        public void setAsc(boolean asc) {
            this.asc = asc;
        }

        public String getSortBy() {
            return sortBy;
        }

        public String getPageSize() {
            return pageSize;
        }

        public String getOffset() {
            return offset;
        }

        public String getCreatorId() {
            return creatorId;
        }
    }

}
