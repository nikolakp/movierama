package com.workable.movierama.registry.services;

public class Model {

    public static class MovieInfo {
        private String id;
        private String title;
        private String description;
        private String creator;
        private long createdAt;
        private long hates;
        private long likes;

        public MovieInfo(String id, String title, String description, String creator, long createdAt, long hates, long likes) {
            this.id = id;
            this.title = title;
            this.description = description;
            this.creator = creator;
            this.createdAt = createdAt;
            this.hates = hates;
            this.likes = likes;
        }

        public MovieInfo() {
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setCreator(String creator) {
            this.creator = creator;
        }

        public void setCreatedAt(long createdAt) {
            this.createdAt = createdAt;
        }

        public void setHates(long hates) {
            this.hates = hates;
        }

        public void setLikes(long likes) {
            this.likes = likes;
        }

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getCreator() {
            return creator;
        }

        public long getCreatedAt() {
            return createdAt;
        }

        public long getHates() {
            return hates;
        }

        public long getLikes() {
            return likes;
        }

    }
}
