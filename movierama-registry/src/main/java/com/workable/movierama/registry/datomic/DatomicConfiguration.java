package com.workable.movierama.registry.datomic;

import clojure.lang.Keyword;
import clojure.lang.PersistentArrayMap;
import datomic.Connection;
import datomic.Database;
import datomic.Peer;
import datomic.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static com.workable.movierama.registry.datomic.DatomicTxFunctions.*;

@Configuration
public class DatomicConfiguration {

    private static final Logger log = LoggerFactory.getLogger(DatomicConfiguration.class);

    @Bean
    public DatomicBean datomicBean(@Value("${datomic.connectionString}") String datomicConnectionString,
                                   @Value("${datomic.schemaFile}") String schemaFile,
                                   @Value("${datomic.queryFile}") String queryFile) throws FileNotFoundException, ExecutionException, InterruptedException {
        String confDir = System.getProperty("confDir");
        Peer.createDatabase(datomicConnectionString);
        Connection conn = Peer.connect(datomicConnectionString);
        FileReader schemaReader = new FileReader(confDir + "/" + schemaFile);
        List schemaTx = (List) Util.readAll(schemaReader).get(0);
        Map map = conn.transact(schemaTx).get();
        log.info("DB Schema creation result {}", map);

        FileReader queryReader = new FileReader(confDir + "/" + queryFile);
        PersistentArrayMap queryMap = (PersistentArrayMap) Util.readAll(queryReader).get(0);

        log.info("Query loading result {}", map);

        Map entityExistsConstraintInstall = ensureEntityExistsConstraintInstall(conn);
        log.info("Existence constraint result {}", entityExistsConstraintInstall);

        Map uniqueConstraintInstall = ensureUniqueConstraintInstall(conn);
        log.info("Unique constraint result {}", uniqueConstraintInstall);

        Map compositeConstraintInstall = ensureCompositeConstraintInstall(conn);
        log.info("Composite constraint result {}", compositeConstraintInstall);

        Map performAction = performAction(conn);
        log.info("Perform Action on movie {}", performAction);

        return new DatomicBean(conn, queryMap);
    }

    public static class DatomicBean {
        private final Connection connection;
        private final Map queryLoader;

        DatomicBean(Connection connection, Map queryLoader) {
            this.connection = connection;
            this.queryLoader = queryLoader;
        }

        public Connection connection() {
            return connection;
        }

        public Database db() {
            return connection.db();
        }

        public Object query(final String key) {
            return queryLoader.get(Keyword.find(key));
        }
    }

}
