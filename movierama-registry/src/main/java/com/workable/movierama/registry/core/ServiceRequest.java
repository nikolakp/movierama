package com.workable.movierama.registry.core;

public class ServiceRequest<In, Principal> {

    private In in;
    private Principal principal;

    public ServiceRequest(In in, Principal principal) {
        this.in = in;
        this.principal = principal;
    }

    public In getIn() {
        return in;
    }

    public Principal getPrincipal() {
        return principal;
    }
}
