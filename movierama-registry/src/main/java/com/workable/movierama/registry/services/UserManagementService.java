package com.workable.movierama.registry.services;

import clojure.lang.PersistentVector;
import com.google.common.hash.Hashing;
import com.workable.movierama.registry.core.ServiceRequest;
import com.workable.movierama.registry.core.ServiceResponse;
import com.workable.movierama.registry.datomic.DatomicConfiguration;
import datomic.Entity;
import datomic.ListenableFuture;
import datomic.Peer;
import datomic.Util;
import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static datomic.Util.list;

@Service
public class UserManagementService {

    private static final Logger log = LoggerFactory.getLogger(UserManagementService.class);

    private final DatomicConfiguration.DatomicBean datomicBean;
    private final SecureRandom secureRandom;

    public UserManagementService(DatomicConfiguration.DatomicBean datomicBean) {
        this.datomicBean = datomicBean;
        secureRandom = new SecureRandom();
    }

    public ServiceResponse<Protocol.LoginResponse> login(ServiceRequest<Protocol.LoginCmd, String> request) {
        Object find = datomicBean.query("findUser");
        Object userId = Peer.query(find, datomicBean.db(), request.getIn().getUsername());
        boolean ok = false;
        if (userId != null && !((PersistentVector) userId).isEmpty()) {
            Entity entity = datomicBean.db().entity(((PersistentVector) userId).get(0));
            final String pwd = entity.get(":userRegistry/pwd").toString();
            ok = Hashing.sha256().hashBytes(request.getIn().getPassword().getBytes()).toString().equals(pwd);
        }
        if (ok) {
            byte bytes[] = new byte[20];
            secureRandom.nextBytes(bytes);
            return new ServiceResponse<>(true, null, null, new Protocol.LoginResponse(Base64.encodeBase64String(bytes)));
        }
        return new ServiceResponse<>(false, "INVALID.CREDENTIALS", null, null);
    }

    public ServiceResponse<String> register(ServiceRequest<Protocol.RegisterCmd, String> request) throws ExecutionException, InterruptedException {
        if (isUserNameInValid(request.getIn().getUsername())) return new ServiceResponse<>(false, "BAD.USERNAME",
                "Username has to comply to the ^[a-z0-9._-]{2,25}$ pattern.", null);
        if (isPasswordInvalid(request.getIn().getPassword())) return new ServiceResponse<>(false, "BAD.PASSWORD",
                "Password has to comply to the (?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,12} pattern.", null);
        try {
            ListenableFuture<Map> transact = datomicBean.connection().transact(list(list(":movierama/ensure-uniqueness", ":userRegistry/username", request.getIn().getUsername()),
                    Util.map(":userRegistry/username", request.getIn().getUsername(), ":userRegistry/pwd", Hashing.sha256().hashBytes(request.getIn().getPassword().getBytes()).toString())));
            Map res = transact.get();
            log.info("Registration successful {}", res);
            return new ServiceResponse<>(true, null, null, null);
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof IllegalStateException) {
                return new ServiceResponse<>(false, ex.getCause().getMessage(), "Username already exists", null);
            }
            throw ex;
        }
    }

    private boolean isUserNameInValid(final String username) {
        return (username == null) || !username.matches("^[a-z0-9._-]{2,25}$");
    }

    private boolean isPasswordInvalid(final String pwd) {
        return (pwd == null) || !pwd.matches("(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,12}");
    }
}
