package com.workable.movierama.registry.services;

import com.google.common.primitives.Ints;
import com.workable.movierama.registry.core.ServiceRequest;
import com.workable.movierama.registry.core.ServiceResponse;
import com.workable.movierama.registry.datomic.DatomicConfiguration;
import datomic.ListenableFuture;
import io.vertx.core.json.Json;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static datomic.Util.list;
import static datomic.Util.map;

@Service
public class MovieRamaService {

    private static final Logger log = LoggerFactory.getLogger(MovieRamaService.class);

    private final DatomicConfiguration.DatomicBean datomicBean;
    private final RestHighLevelClient restClient;

    public MovieRamaService(DatomicConfiguration.DatomicBean datomicBean, RestHighLevelClient restClient) {
        this.datomicBean = datomicBean;
        this.restClient = restClient;
    }

    public ServiceResponse<Void> addMovie(ServiceRequest<Protocol.AddMovieCmd, String> addMovieCmd) throws ExecutionException, InterruptedException {
        if (isInValidMovieInput(addMovieCmd.getIn()))
            return new ServiceResponse<>(false, "BAD.MOVIE.INPUT", "Provide meaningful input.", null);
        String movieId = UUID.randomUUID().toString();
        ListenableFuture<Map> transact = datomicBean.connection().transact(list(
                map(":movieRegistry/id", movieId,
                        ":movieRegistry/title", addMovieCmd.getIn().getTitle(),
                        ":movieRegistry/description", addMovieCmd.getIn().getDescription(),
                        ":movieRegistry/creator", addMovieCmd.getPrincipal(),
                        ":movieRegistry/createdAt", Instant.now().toEpochMilli()
                )));
        Map res = transact.get();
        log.info("Add movie successful {}", res);
        return new ServiceResponse<>(true, null, null, null);
    }

    public ServiceResponse<Void> actionOnMovie(ServiceRequest<Protocol.ActionCmd, String> actionCmd) throws ExecutionException, InterruptedException {
        String movieId = actionCmd.getIn().getMovieId();
        try {
            ListenableFuture<Map> transact = datomicBean.connection().transact(list(
                    list(":movierama/ensure-existence", ":movieRegistry/id", movieId),
                    list(":movierama/ensure-composite", ":movieRegistry/id", movieId, ":movieRegistry/creator", actionCmd.getPrincipal()),
                    list(":movierama/performAction", movieId, actionCmd.getPrincipal(), actionCmd.getIn().getAction())
            ));
            Map res = transact.get();
            log.info("Action on movie {} successful {}", movieId, res);
            return new ServiceResponse<>(true, null, null, null);
        } catch (ExecutionException ex) {
            if (ex.getCause() instanceof IllegalStateException) {
                return new ServiceResponse<>(false, ex.getCause().getMessage(), null, null);
            }
            throw ex;
        }
    }

    public ServiceResponse<Protocol.QueryResponse> fetchMovies(ServiceRequest<Protocol.QueryCmd, String> query) throws IOException {
        Protocol.QueryResponse queryResponse = new Protocol.QueryResponse();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();

        Integer pageSize = query.getIn().getPageSize() == null ? 50 : Ints.tryParse(query.getIn().getPageSize());
        Integer offset = query.getIn().getOffset() == null ? 0 : Ints.tryParse(query.getIn().getOffset());
        if (pageSize > 100) pageSize = 100;

        String sortBy = query.getIn().getSortBy();
        if (sortBy == null) {
            sortBy = "createdAt";
        } else {
            if (!sortBy.equals("hates") && !sortBy.equals("likes") && !sortBy.equals("createdAt")) {
                sortBy = "createdAt";
            }
        }

        String creatorFilter = query.getIn().getCreatorId();
        if (creatorFilter != null) {
            sourceBuilder.query(QueryBuilders.matchQuery("creator", creatorFilter));
        }

        sourceBuilder.from(offset * pageSize);
        sourceBuilder.size(pageSize);
        sourceBuilder.sort(sortBy, query.getIn().isAsc() ? SortOrder.ASC : SortOrder.DESC);

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(sourceBuilder);
        SearchResponse search = restClient.search(searchRequest);

        queryResponse.setCurrentPage((offset / pageSize) + 1);
        queryResponse.setHasNextPage(search.getHits().totalHits > ((offset / pageSize) + 1) * pageSize);

        List<Model.MovieInfo> movies = new ArrayList<>(search.getHits().getHits().length);
        for (int i = 0; i < search.getHits().getHits().length; i++) {
            movies.add(Json.decodeValue(search.getHits().getAt(i).getSourceAsString(), Model.MovieInfo.class));
        }
        queryResponse.setMovies(movies);
        return new ServiceResponse<>(true, null, null, queryResponse);
    }

    private boolean isInValidMovieInput(Protocol.AddMovieCmd addMovieCmd) {
        return addMovieCmd.getTitle() == null || addMovieCmd.getTitle().isEmpty() || addMovieCmd.getDescription() == null || addMovieCmd.getDescription().isEmpty();
    }
}
