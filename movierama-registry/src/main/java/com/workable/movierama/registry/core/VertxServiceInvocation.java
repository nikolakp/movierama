package com.workable.movierama.registry.core;

import io.vertx.ext.web.RoutingContext;

@FunctionalInterface
public interface VertxServiceInvocation<In extends ServiceRequest<?,?>, Out extends ServiceResponse<?>> {
    Out executeService(RoutingContext ctx, In in) throws Exception;
}
