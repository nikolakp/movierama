package com.workable.movierama.registry.core;

public class ServiceResponse<T> {

    private boolean success;
    private String errorCode;
    private String errorDescription;
    private T result;

    public ServiceResponse(boolean success, String errorCode, String errorDescription, T result) {
        this.success = success;
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    String getErrorCode() {
        return errorCode;
    }

    String getErrorDescription() {
        return errorDescription;
    }

    public T getResult() {
        return result;
    }
}
