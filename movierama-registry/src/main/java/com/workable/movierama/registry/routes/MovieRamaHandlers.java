package com.workable.movierama.registry.routes;

import com.workable.movierama.registry.core.ServiceResponse;
import com.workable.movierama.registry.core.VertxInvocationHandler;
import com.workable.movierama.registry.services.MovieRamaService;
import com.workable.movierama.registry.services.Protocol;
import com.workable.movierama.registry.services.UserManagementService;
import io.vertx.core.json.Json;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import org.springframework.stereotype.Component;

@Component
class MovieRamaHandlers {

    private final UserManagementService userManagementService;
    private final MovieRamaService movieRamaService;
    private final VertxInvocationHandler invocationHandler;

    MovieRamaHandlers(UserManagementService userManagementService, MovieRamaService movieRamaService, VertxInvocationHandler invocationHandler) {
        this.userManagementService = userManagementService;
        this.movieRamaService = movieRamaService;
        this.invocationHandler = invocationHandler;
    }

    void handleLogin(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "LOGIN", (ctx1) -> Json.decodeValue(ctx1.getBody(), Protocol.LoginCmd.class), false, (ctx2, serviceRequest) -> {
            ServiceResponse<Protocol.LoginResponse> result = userManagementService.login(serviceRequest);
            if (result.isSuccess()) {
                Session session = ctx.session().regenerateId();
                session.put("csrf", result.getResult().getCsrf());
                session.put("principal", serviceRequest.getIn().getUsername());
                ctx.response().setChunked(true);
                ctx.response().write(Json.encode(result.getResult()));
            }
            return result;
        });
    }

    void handleLogout(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "LOGOUT", (ctx1) -> null, true, (ctx2, serviceRequest) -> {
            ctx.session().destroy();
            return null;
        });
    }

    void handleRegister(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "REGISTER", (ctx1) -> Json.decodeValue(ctx1.getBody(), Protocol.RegisterCmd.class), false,
                (ctx2, serviceRequest) -> userManagementService.register(serviceRequest));
    }

    void handleAddMovie(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "ADD_MOVIE", (ctx1) -> Json.decodeValue(ctx1.getBody(), Protocol.AddMovieCmd.class), true, (ctx2, serviceRequest) -> {
            ServiceResponse<Void> result = movieRamaService.addMovie(serviceRequest);
            if (result.isSuccess()) ctx.response().setStatusCode(202);
            return result;
        });
    }

    void handleHate(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "HATE_MOVIE", (ctx1) -> new Protocol.ActionCmd(ctx.pathParam("movieID"), "hate"), true, (ctx2, serviceRequest) -> {
            ServiceResponse<Void> result = movieRamaService.actionOnMovie(serviceRequest);
            if (result.isSuccess()) ctx.response().setStatusCode(202);
            return result;
        });
    }

    void handleRetract(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "RETRACT_MOVIE", (ctx1) -> new Protocol.ActionCmd(ctx.pathParam("movieID"), "retract"), true, (ctx2, serviceRequest) -> {
            ServiceResponse<Void> result = movieRamaService.actionOnMovie(serviceRequest);
            if (result.isSuccess()) ctx.response().setStatusCode(202);
            return result;
        });
    }

    void handleLike(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "LIKE_MOVIE", (ctx1) -> new Protocol.ActionCmd(ctx.pathParam("movieID"), "like"), true, (ctx2, serviceRequest) -> {
            ServiceResponse<Void> result = movieRamaService.actionOnMovie(serviceRequest);
            if (result.isSuccess()) ctx.response().setStatusCode(202);
            return result;
        });
    }

    void handleGet(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "FETCH_MOVIES", (ctx1) -> new Protocol.QueryCmd(ctx.queryParams().get("creator"),
                ctx.queryParams().get("sortBy"), ctx.queryParams().get("pageSize"), ctx.queryParams().get("offset"),
                "asc".equals(ctx.queryParams().get("order"))), true, (ctx2, serviceRequest) -> {
            ServiceResponse<Protocol.QueryResponse> result = movieRamaService.fetchMovies(serviceRequest);
            if (result.isSuccess()) {
                ctx.response().setChunked(true);
                ctx.response().write(Json.encode(result.getResult()));
            }
            return result;
        });
    }

    void handlePublicGet(RoutingContext ctx) {
        invocationHandler.invocationPipeline(ctx, "FETCH_MOVIES", (ctx1) -> new Protocol.QueryCmd(ctx.queryParams().get("creator"),
                ctx.queryParams().get("sortBy"), ctx.queryParams().get("pageSize"), ctx.queryParams().get("offset"),
                "asc".equals(ctx.queryParams().get("order"))), false, (ctx2, serviceRequest) -> {
            ServiceResponse<Protocol.QueryResponse> result = movieRamaService.fetchMovies(serviceRequest);
            if (result.isSuccess()) {
                ctx.response().setChunked(true);
                ctx.response().write(Json.encode(result.getResult()));
            }
            return result;
        });
    }
}
