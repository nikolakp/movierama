package com.workable.movierama.registry.datomic;


import datomic.Connection;

import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static datomic.Peer.function;
import static datomic.Peer.tempid;
import static datomic.Util.*;

/**
 * @author nikolakp
 * Datomic Transactor triggers for Movierama Database

 */
public class DatomicTxFunctions {

    public static Map ensureCompositeConstraintInstall(Connection conn) throws InterruptedException, ExecutionException {
        String source = "import java.util.List;\nimport static datomic.Peer.query;\nimport static datomic.Peer.toT;\n" +
                "import static datomic.Util.list;\nimport static datomic.Util.map;\nimport static datomic.Util.read;\n" +
                "String compositeQuery = \"[:find [?e ?t1 ?t2] \" +\n" +
                "            \":in $ ?k1 ?v1 ?k2 ?v2 \" +\n" +
                "            \":where \" +\n" +
                "            \"[?e ?k1 ?v1 ?t1] \" +\n" +
                "            \"[?e ?k2 ?v2 ?t2]]\";\n" +
                "    List<Object> conflict = query(compositeQuery, db, k1, v1, k2, v2);\n" +
                "    if (conflict != null && !conflict.isEmpty()) {\n" +
                "        throw new IllegalStateException(\"composite.constraint.violation\");\n" +
                "    } else {\n" +
                "        return list();\n" +
                "    }\n";
        datomic.functions.Fn ensureComposite =
                function(map(read(":lang"), "java",
                        read(":params"), list(read("db"), read("k1"), read("v1"), read("k2"), read("v2")),
                        read(":code"), source));
        Future<Map> txResult =
                conn.transact(list(map(read(":db/id"), tempid(read(":db.part/user")),
                        read(":db/fn"), ensureComposite,
                        read(":db/ident"), read(":movierama/ensure-composite"),
                        read(":db/doc"), "Create an entity with k1=v1, k2=v2, throwing if such an entity already exists")));
        return txResult.get();
    }

    public static Map ensureUniqueConstraintInstall(Connection conn) throws InterruptedException, ExecutionException {
        String source = "import java.util.List;\nimport static datomic.Peer.query;\nimport static datomic.Peer.toT;\n" +
                "import static datomic.Util.list;\nimport static datomic.Util.map;\nimport static datomic.Util.read;\n" +
                "String uniqueQuery = \"[:find [?e ?t1] \" +\n" +
                "            \":in $ ?k1 ?v1 \" +\n" +
                "            \":where \" +\n" +
                "            \"[?e ?k1 ?v1 ?t1]]\";\n" +
                "    List<Object> conflict = query(uniqueQuery, db, k1, v1);\n" +
                "    if (conflict != null && !conflict.isEmpty()) {\n" +
                "        throw new IllegalStateException(\"unique.constraint.violation\");\n" +
                "    } else {\n" +
                "        return list();\n" +
                "    }\n";
        datomic.functions.Fn ensureExists =
                function(map(read(":lang"), "java",
                        read(":params"), list(read("db"), read("k1"), read("v1")),
                        read(":code"), source));
        Future<Map> txResult =
                conn.transact(list(map(read(":db/id"), tempid(read(":db.part/user")),
                        read(":db/fn"), ensureExists,
                        read(":db/ident"), read(":movierama/ensure-uniqueness"),
                        read(":db/doc"), "Create an entity only if an entity with k1=v1 doesn't exist")));
        return txResult.get();
    }


    public static Map ensureEntityExistsConstraintInstall(Connection conn) throws InterruptedException, ExecutionException {
        String source = "import java.util.List;\nimport static datomic.Peer.query;\nimport static datomic.Peer.toT;\n" +
                "import static datomic.Util.list;\nimport static datomic.Util.map;\nimport static datomic.Util.read;\n" +
                "String existenceQuery = \"[:find [?e ?t1] \" +\n" +
                "            \":in $ ?k1 ?v1 \" +\n" +
                "            \":where \" +\n" +
                "            \"[?e ?k1 ?v1 ?t1]]\";\n" +
                "    List<Object> exists = query(existenceQuery, db, k1, v1);\n" +
                "    if (exists == null || exists.isEmpty()) {\n" +
                "        throw new IllegalStateException(\"existence.constraint.violation\");\n" +
                "    } else {\n" +
                "        return list();\n" +
                "    }\n";
        datomic.functions.Fn ensureExists =
                function(map(read(":lang"), "java",
                        read(":params"), list(read("db"), read("k1"), read("v1")),
                        read(":code"), source));
        Future<Map> txResult =
                conn.transact(list(map(read(":db/id"), tempid(read(":db.part/user")),
                        read(":db/fn"), ensureExists,
                        read(":db/ident"), read(":movierama/ensure-existence"),
                        read(":db/doc"), "Create an entity only if an entity with k1=v1 already exists")));
        return txResult.get();
    }

    public static Map performAction(Connection conn) throws InterruptedException, ExecutionException {
        String source = "import clojure.lang.PersistentVector;import datomic.Database;\nimport datomic.Entity;\nimport java.util.List;\nimport static datomic.Peer.query;\nimport static datomic.Peer.toT;\n" +
                "import static datomic.Util.list;\nimport static datomic.Util.map;\nimport static datomic.Util.read;\n" +
                "String currentMaxTxOnMovie = \"[:find ?e (max ?tx) ?a ?v \" +\n" +
                "            \":in $ ?movieId ?editor \" +\n" +
                "            \":where \" +\n" +
                "            \"[?e :movieRegistry/movieId ?movieId] [?e :movieRegistry/userId ?editor] [?e ?a ?v ?tx]]\";\n" +
                "    List<Object> exists = query(currentMaxTxOnMovie, db, movieId, editor);\n" +
                "    if (exists != null && !exists.isEmpty()) {\n" +
                "         int index = ((PersistentVector)exists).size()-1;                               \n"+
                "         Entity entity = ((Database)db).entity(((PersistentVector)(exists).get(index)).get(0)); \n" +
                "         String currentAction = entity.get(\":movieRegistry/action\").toString(); \n" +
                "         String transition = null;                                     \n" +
                "         if (currentAction.equals(\"hate\") && action.equals(\"hate\"))transition=\"0\";           \n" +
                "         else if (currentAction.equals(\"like\") && action.equals(\"like\"))transition=\"0\";      \n" +
                "         else if (currentAction.equals(\"hate\") && action.equals(\"like\"))transition=\"+l-h\";   \n" +
                "         else if (currentAction.equals(\"like\") && action.equals(\"hate\"))transition=\"+h-l\";   \n" +
                "         else if (currentAction.equals(\"like\") && action.equals(\"retract\"))transition=\"-l\";  \n" +
                "         else if (currentAction.equals(\"hate\") && action.equals(\"retract\"))transition=\"-h\";  \n" +
                "         else if (currentAction.equals(\"retract\") && action.equals(\"like\"))transition=\"+l\";  \n" +
                "         else if (currentAction.equals(\"retract\") && action.equals(\"hate\"))transition=\"+h\";  \n" +
                "         else if (currentAction.equals(\"retract\") && action.equals(\"retract\"))transition=\"0\";  \n" +
                "           System.out.println(\"current=>\" + currentAction + \"existing=>\" + transition);\n" +
                "        return list(map(\":movieRegistry/userId\", editor, \":movieRegistry/movieId\", movieId, " +
                "         \":movieRegistry/action\", action, \":movieRegistry/transition\", transition));\n" +
                "    } else {\n" +
                "         String transition = null;                                        \n" +
                "         if (action.equals(\"hate\"))transition=\"+h\";                   \n" +
                "         else if (action.equals(\"like\"))transition=\"+l\";              \n" +
                "         else if (action.equals(\"retract\"))transition=\"0\";            \n" +
                "           System.out.println(\"current=>null\" + \",new=>\" + transition);\n" +
                "        return list(map(\":movieRegistry/userId\", editor, \":movieRegistry/movieId\", movieId, " +
                "\":movieRegistry/action\", action, \":movieRegistry/transition\", transition));\n" +
                "    }\n";

        datomic.functions.Fn performAction =
                function(map(read(":lang"), "java",
                        read(":params"), list(read("db"),
                                read("movieId"), read("editor"), read("action")),
                        read(":code"), source));
        Future<Map> txResult =
                conn.transact(list(map(read(":db/id"), tempid(read(":db.part/user")),
                        read(":db/fn"), performAction,
                        read(":db/ident"), read(":movierama/performAction"),
                        read(":db/doc"), "Calculate Transition")));
        return txResult.get();
    }
}
