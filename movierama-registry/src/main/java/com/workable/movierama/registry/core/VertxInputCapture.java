package com.workable.movierama.registry.core;

import io.vertx.ext.web.RoutingContext;

@FunctionalInterface
public interface VertxInputCapture<Target> {
    Target fromContext(RoutingContext ctx);
}
