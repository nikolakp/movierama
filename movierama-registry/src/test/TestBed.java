package com.workable.movierama.registry.services;

import clojure.lang.Keyword;
import clojure.lang.PersistentArrayMap;
import clojure.lang.PersistentVector;
import datomic.Connection;
import datomic.ListenableFuture;
import datomic.Peer;
import datomic.Util;
import io.vertx.core.json.Json;
import org.apache.http.HttpHost;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import static com.workable.movierama.registry.datomic.DatomicTxFunctions.*;
import static datomic.Util.list;
import static datomic.Util.map;

public class TestBed {

    private static final Logger log = LoggerFactory.getLogger(TestBed.class);

    public static void main(String[] args) throws IOException {
        RestHighLevelClient x = new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("0.0.0.0", 9200, "http")));

        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.from(0);
        sourceBuilder.size(5);
        sourceBuilder.sort("createdAt", SortOrder.ASC);
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(sourceBuilder);
        SearchResponse search = x.search(searchRequest);
        System.out.println();
        Model.MovieInfo movieInfo = Json.decodeValue(search.getHits().getAt(0).getSourceAsString(), Model.MovieInfo.class);

        System.out.println(search);

    }

    public static void main1(String[] args) throws FileNotFoundException, ExecutionException, InterruptedException {

        String x = "datomic:sql://movierama?jdbc:postgresql://0.0.0.0:5432/datomic?user=datomic&password=datomic";
        Peer.createDatabase(x);
        Connection conn = Peer.connect(x);

        FileReader schemaReader0 = new FileReader("/home/vncuser/IdeaProjects/workable-movierama/configuration/movierama-registry/movierama-registry-schema.edn");
        List schemaTx0 = (List) Util.readAll(schemaReader0).get(0);
        Map map = conn.transact(schemaTx0).get();
        log.info("DB Schema creation result {}", map);


        FileReader queryReader0 = new FileReader("/home/vncuser/IdeaProjects/workable-movierama/configuration/movierama-registry/movierama-registry-queries.edn");
        PersistentArrayMap queryMap0 = (PersistentArrayMap) Util.readAll(queryReader0).get(0);


        FileReader schemaReader = new FileReader("/home/vncuser/IdeaProjects/workable-movierama/configuration/materialised-view-feeder/materialised-view-feeder-schema.edn");
        List schemaTx = (List) Util.readAll(schemaReader).get(0);
        Map map0 = conn.transact(schemaTx).get();
        log.info("DB Schema creation result {}", map);


        FileReader queryReader = new FileReader("/home/vncuser/IdeaProjects/workable-movierama/configuration/materialised-view-feeder/materialised-view-feeder-queries.edn");
        PersistentArrayMap queryMap = (PersistentArrayMap) Util.readAll(queryReader).get(0);

        Map entityExistsConstraintInstall = ensureEntityExistsConstraintInstall(conn);
        log.info("Existence constraint result {}", entityExistsConstraintInstall);

        Map uniqueConstraintInstall = ensureUniqueConstraintInstall(conn);
        log.info("Unique constraint result {}", uniqueConstraintInstall);

        Map compositeConstraintInstall = ensureCompositeConstraintInstall(conn);
        log.info("Composite constraint result {}", compositeConstraintInstall);

        Map performAction = performAction(conn);
        log.info("Max Tx finder install {}", performAction);

        Random rnd = new Random();
        String movieId = UUID.randomUUID().toString();

        ListenableFuture<Map> transact = conn.transact(list(
                map(":movieRegistry/id", movieId,
                        ":movieRegistry/title", "Gladiator",
                        ":movieRegistry/description", "Gladiator",
                        ":movieRegistry/creator", "Ruler" + rnd.nextInt(),
                        ":movieRegistry/createdAt", Instant.now().toEpochMilli()
                )));
        Map res = transact.get();
        log.info("Add movie successful {}", res);

        ListenableFuture<Map> transact1 = conn.transact(list(
                list(":movierama/ensure-existence", ":movieRegistry/id", movieId),
                list(":movierama/ensure-composite", ":movieRegistry/id", movieId, ":movieRegistry/creator", "Troy"),
                list(":movierama/performAction", movieId, "Troy", rnd.nextGaussian() > 0.4 ? "like" : "hate")
        ));
        Map res1 = transact1.get();
        log.info("Action on movie {} successful {}", movieId, res1);

        Object txs = queryMap.get(Keyword.find("txs"));
        PersistentVector newTxs = Peer.query(txs, conn.db(), -1);

        System.out.println(conn.db().entity(((PersistentVector) newTxs.get(newTxs.size() - 1)).get(0)).get(":movieRegistry/transition"));
    }
}
