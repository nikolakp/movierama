#!/bin/bash

psql -f /docker-entrypoint-initdb.d/sql/postgres-db.sql -U postgres

psql -f /docker-entrypoint-initdb.d/sql/postgres-table.sql -U postgres -d datomic

psql -f /docker-entrypoint-initdb.d/sql/postgres-user.sql -U postgres -d datomic